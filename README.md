# TaxFix Home Assignment

Goal: create a small pipeline to ingest data from an external API, anonymize the
data, store it in a database and generate a report.

## Project structure explained

```text
taxfix_home_assignment/ # app folder
    __main__.py         # app entry point with args and logging
    process.py          # faker API call and logic    
    settings.py         # Django ORM settings
    manage.py           # Django ORM initialization
    db/                 # database related files
        analysis.ipynb  # jupyter notebook with SQL answers
        scratches.sql   # draft of plain SQL commands
        db.sqlite3      # database with 10k records
        models.py       # Django ORM model to reflect ingested data
        migrations/     # Django database migrations
```

## Setup
```sh
# Install dependencies
pipenv install --dev

# Setup pre-commit and pre-push hooks
pipenv run pre-commit install -t pre-commit
pipenv run pre-commit install -t pre-push

# initialize the database
python manage.py makemigrations db; python manage.py migrate
```

## Usage
Fetching new data:
```shell script
# Build new docker image
docker build -t taxfix_home_assignment .

# run container and mount SQLite as a volume (ingest 100 records)
docker run --rm -v <ABSOLUTE_PATH>/taxfix_home_assignment/taxfix_home_assignment/db:/home/appuser/taxfix_home_assignment/db taxfix_home_assignment --batch_size 100
```
Other commands :
```shell script
# to reset the database (if needed)
python taxfix_home_assignment/manage.py flush

# check tests
pytest
```

## Jupyter with data analisys
I've built this jupyter notebook to answer the SQL questions from the assignment with the database created
```shell script
jupyter notebook taxfix_home_assignment/db/analysis.ipynb
```

```text
Which percentage of users live in Germany and use Gmail as an email provider?
cmd = '''
select
printf("%.4f", count(*) * 1.0 / (select count(*) from db_person)) || "%" as germans_using_gmail
from db_person
where country = 'Germany' and email_provider = '@gmail.com'
'''
%sql $cmd

germans_using_gmail
0.0005%
```

```text
Which are the top countries in our database that use Gmail as an email provider?
cmd = '''
select * from (
    with cte as (
        select
            country,
            count(*) total_email_users
        from db_person
        where email_provider = '@gmail.com'
        group by country
        order by total_email_users desc
    )
    select
        row_number() over (order by total_email_users desc) as email_rank,
        cte.*
    from cte
) a where email_rank <= 3;
'''
%sql $cmd

country	count(*)
Moldova	    14
Grenada	    14
Greenland   14
```

```text
How many people over 60 years use Gmail as an email provider?
cmd = '''
select count(*) from db_person where email_provider = '@gmail.com' and age_range_lower >= 60;
'''
%sql $cmd

count(*)
553
```

Check other additional analisys in the notebook, like top 5 email providers
![](docs/pie-chart.png?raw=true)

## Tasks / Further improvements
- [X] implement more tests
- [ ] use Django test functionality instead of pytest
- [ ] add docker-compose (enable [relative path](https://stackoverflow.com/questions/46147839/docker-how-can-i-have-sqlite-db-changes-persist-to-the-db-file) to mount volumes )
- [ ] best practice would be saving raw (but already MASKED!) json data first, in the case we need to retry loading
- [ ] wrap jupyter nootebook to also run inside docker
- [ ] need to fix "too many dependencies" to improve docker container creation
    - after installing seaborn, ipython-sql and pandas, something unexpected happened with the .venv, that added more than 80 new dependencies. This is causing docker container creation to be very slow. Need to further investigate to understand

## Credits
This project was created with Cookiecutter
- original reference: [sourcery-ai/python-best-practices-cookiecutter](https://github.com/sourcery-ai/python-best-practices-cookiecutter)
- My improved template: [vsf-pocs/python_cookiecutter.git](https://gitlab.com/vsf-pocs/python_cookiecutter)
    - improvements: gitlab-ci, docker, django-orm
