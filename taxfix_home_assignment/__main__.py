import logging
from argparse import ArgumentParser

from taxfix_home_assignment.process import process_faker_batch


def parse_args(args):
    parser = ArgumentParser()
    parser.add_argument(
        "--batch_size",
        required=True,
        help="number of persons to be requested from Faker API",
    )

    return parser.parse_args(args)


def main(args):
    options = parse_args(args)
    process_faker_batch(int(options.batch_size))


if __name__ == "__main__":
    import sys

    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s.%(msecs)03d %(levelname)s %(module)s - %(funcName)s: %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
    )

    main(sys.argv[1:])
