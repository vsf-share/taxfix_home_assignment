from datetime import datetime
from math import floor

from django.db import models


class Person(models.Model):
    """
    Sample data:
        "age_range": "30-40",
        "email_provider": "@metz.net",
        "gender": "female",
        "city": "Lavadaview",
        "country": "Turkmenistan",
        "county_code": "SI"
    """

    age_range = models.CharField(max_length=10)
    age_range_lower = models.IntegerField()
    age_range_upper = models.IntegerField()
    email_provider = models.CharField(max_length=250)
    gender = models.CharField(max_length=6)
    city = models.CharField(max_length=250)
    country = models.CharField(max_length=100)
    county_code = models.CharField(max_length=2)
    created_at = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        # generalizing data to mask personal information
        # birthday to age range:
        birthday = datetime.strptime(self.age_range, "%Y-%m-%d")
        years_diff = (datetime.today() - birthday).days / 365
        self.age_range_lower = floor(years_diff / 10) * 10
        self.age_range_upper = self.age_range_lower + 9
        self.age_range = (
            f"{str(self.age_range_lower).zfill(2)}-{str(self.age_range_upper).zfill(2)}"
        )

        # cleaning email identifier and keeping only the provider:
        if self.email_provider:
            self.email_provider = self.email_provider[self.email_provider.index("@") :]

        super(Person, self).save(*args, **kwargs)

    def __str__(self):
        return f"between {self.age_range} years old, from {self.country}"
