select count(*) from db_person;
select * from db_person;

select age_range, count(*) from db_person group by age_range;
select country, count(*) from db_person group by country;

-- SQL 1
select
printf("%.4f", count(*)*1.0 / (select count(*) from db_person)) || "%" as germans_using_gmail
from db_person
where country = 'Germany' and email_provider = '@gmail.com';

-- SQL 2
select country, count(*)
from db_person where email_provider = '@gmail.com'
group by country order by 2 desc limit 4;

-- SQL 3
select count(*) from db_person where email_provider = '@gmail.com' and age_range_lower >= 60;

---------------------------
-- Additional SQLs
---------------------------

-- Users in Germany and stats over email usage
with cte as (
    select
        country, id, age_range, gender,
        email_provider,
        count(*) over (partition by email_provider) as total_email_users,
        printf("%.2f", count(*) over (partition by email_provider) * 1.0 / count(*) over () * 100) || "%"  as email_provider_market_share
    from db_person
    where country = 'Germany'
    order by total_email_users desc
)
select
    dense_rank() over (order by total_email_users desc) as email_rank,
    cte.*
from cte;


select * from (
    with cte as (
        select
            country,
            count(*) total_email_users
        from db_person
        where email_provider = '@gmail.com'
        group by country
        order by total_email_users desc
    )
    select
        row_number() over (order by total_email_users desc) as email_rank,
        cte.*
    from cte
) a where email_rank <= 3;
