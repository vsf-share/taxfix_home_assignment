# setting up django with the following import
import json
import logging

import requests

from taxfix_home_assignment import manage  # noqa: F401
from taxfix_home_assignment.db.models import Person  # noqa: F401


def request_faker_api(batch_size):
    url = f"https://fakerapi.it/api/v1/persons?_quantity={batch_size}"
    response = requests.get(url)
    return json.loads(response.text)


def load_persons_into_db(data):
    for person in data["data"]:
        Person.objects.create(
            gender=person["gender"],
            city=person["address"]["city"],
            country=person["address"]["country"],
            county_code=person["address"]["county_code"],
            email_provider=person["email"],
            age_range=person["birthday"],
        )


def process_faker_batch(input_batch_size: int):
    # faker API is limited to a max of 1000 records per request
    # thus, if passed batch_size is more than 1000, we need to split in many requests

    logging.info("started process")

    while input_batch_size > 0:
        request_batch_size = min(1000, input_batch_size)

        data = request_faker_api(request_batch_size)
        load_persons_into_db(data)

        input_batch_size -= request_batch_size

    logging.info("finished process")
