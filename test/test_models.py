from datetime import datetime

from django.test import TestCase

from taxfix_home_assignment import manage  # noqa: F401
from taxfix_home_assignment.db.models import Person  # noqa: F401


class PersonModelTest(TestCase):
    def test_new_person(self):
        self.person = Person.objects.create(
            gender="M",
            city="Vitoria",
            country="Brazil",
            county_code="BR",
            email_provider="test@gmail.com",
            age_range="2010-07-21",
        )
        self.assertTrue(Person.objects.filter(id=self.person.id))

    def test_cleaned_fields(self):
        self.person = Person.objects.create(
            gender="M",
            city="Vitoria",
            country="Brazil",
            county_code="BR",
            email_provider="vitor@gmail.com",
            age_range="1995-07-21",
        )
        self.assertEqual(self.person.gender, "M")
        self.assertEqual(self.person.city, "Vitoria")
        self.assertEqual(self.person.country, "Brazil")
        self.assertEqual(self.person.county_code, "BR")
        self.assertEqual(self.person.email_provider, "@gmail.com")
        self.assertEqual(self.person.age_range, "20-29")

    def test_age_grouping(self):
        today = datetime.today()
        self.assertEqual(
            Person.objects.create(age_range=f"{today.strftime('%Y-%m-%d')}").age_range,
            "00-09",
        )

        self.assertEqual(
            Person.objects.create(age_range="2022-06-01").age_range, "00-09"
        )
        self.assertEqual(
            Person.objects.create(age_range="2012-09-01").age_range, "00-09"
        )
        self.assertEqual(
            Person.objects.create(age_range="2010-07-21").age_range, "10-19"
        )
        self.assertEqual(
            Person.objects.create(age_range="2012-01-01").age_range, "10-19"
        )
        self.assertEqual(
            Person.objects.create(age_range="2002-02-21").age_range, "20-29"
        )
        self.assertEqual(
            Person.objects.create(age_range="2000-07-21").age_range, "20-29"
        )
