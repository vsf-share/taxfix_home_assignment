import json

import requests_mock
from django.test import TestCase

from taxfix_home_assignment import manage  # noqa: F401
from taxfix_home_assignment.db.models import Person  # noqa: F401
from taxfix_home_assignment.process import (
    load_persons_into_db,
    process_faker_batch,
    request_faker_api,
)


class FakerApiTest(TestCase):
    @requests_mock.Mocker()
    def setUp(self, m) -> None:
        self.json_response = {
            "status": "OK",
            "code": 200,
            "total": 1,
            "data": [
                {
                    "id": 1,
                    "firstname": "Gussie",
                    "lastname": "Botsford",
                    "email": "donny.macejkovic@greenfelder.org",
                    "phone": "+9491318391954",
                    "birthday": "2021-07-19",
                    "gender": "male",
                    "address": {
                        "id": 0,
                        "street": "2917 Cleveland Point",
                        "streetName": "Schiller Brooks",
                        "buildingNumber": "796",
                        "city": "Skilesborough",
                        "zipcode": "87222-9485",
                        "country": "Bouvet Island (Bouvetoya)",
                        "county_code": "LB",
                        "latitude": -84.666079,
                        "longitude": -97.3854,
                    },
                    "website": "http://hoeger.org",
                    "image": "http://placeimg.com/640/480/people",
                },
            ],
        }

        m.get(
            "https://fakerapi.it/api/v1/persons?_quantity=1",
            text=json.dumps(self.json_response),
        )
        self.data = request_faker_api(1)

    def test_request_faker_api(self):
        self.assertIsNotNone(self.data)
        self.assertEqual(self.data["status"], "OK")
        self.assertEqual(self.data["code"], 200)
        self.assertEqual(self.data["total"], 1)

    def test_load_persons_into_db(self):
        load_persons_into_db(self.data)
        self.assertTrue(Person.objects.exists())

    @requests_mock.Mocker()
    def test_process_faker_batch(self, m):
        m.get(
            "https://fakerapi.it/api/v1/persons?_quantity=1",
            text=json.dumps(self.json_response),
        )

        with self.assertLogs() as captured:
            process_faker_batch(1)
            self.assertEqual(len(captured.records), 2)
            self.assertEqual(captured.records[1].getMessage(), "finished process")
